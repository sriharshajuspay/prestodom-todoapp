# PrestoDOM Todo App demo

This repo is an example of PrestoDOM where we go about creating a complete Todo application, with the goal that the end result should allow to store the todos in the local storage, and also retrieve them.

The repo will serve with each commit, and with each commit, this readme serves as a tutorial as what needs to be done from the previous step to arrive at this step.

## Creating a new project

For this to work, we assume the PureScript to be 0.11.7 and Node JS at least 9.5.0 is installed. Please do not update PureScript to 0.12 yet because the dependency libraries are not updated.

  - Create a PureScript project.

        $ pulp init

  - Also initialize NPM here.

        $ npm init

  - Install dependencies Presto Core (Flows) and PrestoDOM (UI).

        $ bower i https://github.com/juspay/purescript-presto.git#presto-dom-v0.2 --save
        $ bower i https://bitbucket.org/juspay/purescript-presto-dom.git#v0.2 --save

  - Install the PrestoUI and Ramda (PrestoDOM dependencies from NPM).

        $ npm i presto-ui --save
        $ npm i ramda --save

  - Install WebPack and Babel for transpiling JS sources and creating a single bundle file.

        $ npm i webpack webpack-cli --save-dev
        $ npm i babel-core babel-loader babel-preset-env --save-dev

  - Install WebPack Development Server to see updated changes.

        $ npm i webpack-dev-server --save-dev

  - Configure WebPack with the configuration file. See the webpack.config.js file in this repo.

  - Install concurrently so that you can run both PSC and WebPack in a single terminal.

        $ npm i concurrently --save-dev

  - Create NPM scripts to be able to start the server. See the package.json file.

  - Create the dist/index.html that loads just the index_bundle.js file.

  - Create the index.js file in root so that you can start calling the PureScript code.

That is all there is to create the project with the dependencies.

## Running the project

To run the project, execute:

    $ npm start

## Setting up Presto Flow Framework

Remove the Main module in your source, and then create a new module named as Core. This is where everything starts. Also be sure to change the reference in index.js file. That will now become as:

~~~javascript
const purescript = require("./output/Core/index.js");

purescript.main();
~~~

This will now give out an error, as main doesn't yet exist in the project yet. For now, create a main that does nothing there. Let's patch it up later.

~~~purescript
main :: forall e. Eff (AppEffects e) Unit
main = launchApp
~~~

Now go and create a new function called as launchApp which takes care of actual launching of the app. However, before doing that, there is some terminology that we need to know.

  - **Flow**  
    A flow is, well, a flow. How the app works in a high level logic. This also needs to be understood by people who doesn't know how the code works. It is a overall flow of user interaction.

  - **Runner**  
    A runner is an interpreter (don't ask all the Free monad stuff here, but all you need to know is that there is a Flow language for which an interpreter exists) for the Flows, well part of the application we are going to build.

    We need runners for UI, API calls and application permissions. These are separated because Presto framework allows it to be used with any other UI framework.

There is still some stuff that is omitted from explanation, but we will take them when we are actually going to use them.

Let's get started by creating the launchApp function.

~~~purescript
launchApp :: forall e. Eff (AppEffects e) Unit
launchApp = do
  let runtime = Runtime uiRunner permissionRunner apiRunner
  let freeFlow = evalStateT (run runtime appFlow)
  launchAff_ (makeVar empty >>= freeFlow)

  where
    -- ......
~~~

Whew, there is too much of stuff. But still, it is only simple if you really try to understand.

First we are creating the runtime, by using the `Runtime` data constructor and passing the runners for UI, permissions and API calls that we discussed before in the terminology. Then we are running the framework by using the runtime we created and the `appFlow`.

Finally we run it asynchronously. The `appFlow` is simply a function which does nothing for now.

~~~purescript
appFlow :: Flow Unit
appFlow = doAff (log "Hello World") *> pure unit
~~~

Now comes the main part, creating these individual runtimes that we are passing to the Runtime data constructor. For now, they are fairly empty and does not do anything, so I'll just list them and get over with.

~~~purescript
uiRunner :: UIRunner
uiRunner screen = makeAff (\callback -> (\_ _ -> pure "") (Right >>> callback) screen *> pure nonCanceler)

apiRunner :: APIRunner
apiRunner request = makeAff (\callback -> (Right >>> callback) "Test" *> pure nonCanceler)

permissionRunner :: PermissionRunner
permissionRunner = PermissionRunner permissionCheckRunner permissionTakeRunner

permissionCheckRunner :: PermissionCheckRunner
permissionCheckRunner _ = makeAff (\callback -> (Right >>> callback) PermissionGranted *> pure nonCanceler)

permissionTakeRunner :: PermissionTakeRunner
permissionTakeRunner _ = makeAff (\callback -> (Right >>> callback) [] *> pure nonCanceler)
~~~

We will deal with each of them when we finally and actually start to using them. With this, the boilerplate setup is mostly done. In the next part, let's get started with PrestoDOM and create a simple screen.

## Basics of PrestoDOM library

PrestoDOM is a PureScript wrapper around PrestoUI, a JS based native UI solution. It is based on a VDOM (Virtual Dom) structure and makes writing UIs very easy. It is built around the concept of a screen, and follows an Action-Eval form just like your flows.

~~~purescript
type Screen action st eff retAction =
  { initialState :: st
  , view :: (action -> PropEff eff) -> st -> PrestoDOM (PropEff eff) Void
  , eval :: action -> st -> Eval eff action retAction st
  }
~~~

As you can see in the above snippet, this is the type of `Screen` in PrestoDOM library.

The `Screen` type takes four phantom types, which collectively denote the behaviour of your screen. They are as follows:

  - `action`  
    This is a data type of all the possible actions that might happen inside the screen.

  - `st`  
    This is the state data type. Every screen will have some state, and the type of the state used by your screen goes into st phantom type.

  - `eff`  
    Any additional effects required by the caller.

  - `retAction`  
    This is the data type of the action that your screen returns to the flow, as the result of running this screen.

The Screen type also contains a record of three members &mdash; `initialState`, `view` and `eval` where view is the function that takes a state and generates a PrestoDOM ready to be rendered, and eval function takes an action and a state and evaluates it.

Now there is another argument of type `(action -> PropEff eff)` to the view. This is a function we call as push. This function is used by event properties, and calling this function will cause the eval function to execute.

## The TODO app plan

We are going to make a very small app with just three screens &mdash; a `MainScreen`, `AddTodoScreen` and `ViewTodoScreen`. Since you already got the perspective, I'm just going to leave the diagram for design (I knew it's looking horrible, but ignore my primitive drawing skills) here for you.

![application Plan](app-plan.jpeg)

The main screen is a list of the todo items that are already stored in the app, with an add button that allows to create a new item.

The rest screens contains the summary and details (editable in AddTodoScreen but not in view screen). For now, let's start with the main screen, and go on refactoring into the components later when we need to reuse the views.

Enough of theory and let's now get into practice.

## Setting up PrestoDOM library

Remember the `uiRunner` function that we wrote in the previous sections which currently does nothing? We're still going to keep it as it is. We are not at all going to use it, not only now but also in the future with the PrestoDOM library.

Then why did we have the UIRunner concept? It is only a bridge between the JavaScript UI libraries and Presto Core library. But now, we are not going to need it, because we are using PrestoDOM which itself is a PureScript library. We don't need that bridge because we are not going into JavaScript world directly.

However, we need to tell the Presto Core library to initialize the UI using PrestoDOM. So for that purpose, we change the `appFlow` which currently does nothing into the following.

~~~purescript
appFlow :: Flow Unit
appFlow = initUI *> mainScreen
~~~

That's it, we have our PrestoDOM library set up and ready to use. Now it is time for us to create the screen that we can run.

## MainScreen

Go ahead and create a new module called as `UI.MainScreen` in the project. Now start writing the types for Action inside the screen and the state it needs.

~~~purescript
data Action = MainScreenAction

type State = {}

initialState :: State
initialState = {}
~~~

Now write the view and eval functions. See the source file for their contents. Finally create the screen function that returns a record of type Screen.

~~~purescript
screen :: forall eff. Screen Action State eff Unit
screen =
  { initialState
  , eval
  , view
  }
~~~

Now we can call it from anywhere by using the `runScreen` function. Since we are using it from the Flow, make sure you import it from the Presto Core and not from PrestoDOM.

Now create a new module called as `UI.Flow` where we are going to write main flows that call the screen.

~~~purescript
mainScreen :: Flow Unit
mainScreen = runScreen MainScreen.screen
~~~

Now compile this, and you will be able to see the PrestoDOM screen running in your browser. In the next part, we will be adding some todo items and get them rendering in the screen.
