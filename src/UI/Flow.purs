module UI.Flow where

import Prelude

import Presto.Core.Types.Language.Flow (Flow, runScreen)
import UI.MainScreen as MainScreen

mainScreen :: Flow Unit
mainScreen = runScreen MainScreen.screen
