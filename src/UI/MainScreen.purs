module UI.MainScreen where

import Prelude

import PrestoDOM (Eval, Gravity(..), Length(..), Padding(..), PrestoDOM, PropEff, Screen, background, color, continue, gravity, height, linearLayout, padding, root, text, textSize, textView, width)

data Action = MainScreen

type State =
  {
  }

screen :: forall eff. Screen Action State eff Unit
screen =
  { initialState
  , eval
  , view
  }

initialState :: State
initialState =
  {
  }

eval :: forall eff. Action -> State -> Eval eff Action Unit State
eval _ state = continue state

view :: forall w eff. (Action -> PropEff eff) -> State -> PrestoDOM (PropEff eff) w
view _ _ =
  linearLayout
    [ width MATCH_PARENT
    , height MATCH_PARENT
    , background "#2C3A47"
    , root true
    , padding $ Padding 20 50 20 50
    ]
    [ textView
        [ color "#F8EFBA"
        , textSize 28
        , text "My TODOs"
        , width MATCH_PARENT
        , gravity CENTER
        ]
    ]
