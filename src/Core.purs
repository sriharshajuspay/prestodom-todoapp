module Core where

import Prelude

import Control.Monad.Aff (launchAff_, makeAff, nonCanceler)
import Control.Monad.Aff.AVar (makeVar)
import Control.Monad.Eff (Eff)
import Control.Monad.State (evalStateT)
import Data.Either (Either(..))
import Data.StrMap (empty)
import Presto.Core.Language.Runtime.API (APIRunner)
import Presto.Core.Language.Runtime.Interpreter (PermissionCheckRunner, PermissionRunner(..), Runtime(..), UIRunner, PermissionTakeRunner, run)
import Presto.Core.Types.App (AppEffects)
import Presto.Core.Types.Language.Flow (Flow, initUI)
import Presto.Core.Types.Permission (PermissionStatus(..))
import UI.Flow (mainScreen)

appFlow :: Flow Unit
appFlow = initUI *> mainScreen

launchApp :: forall e. Eff (AppEffects e) Unit
launchApp = do
  let runtime = Runtime uiRunner permissionRunner apiRunner
  let freeFlow = evalStateT (run runtime appFlow)
  launchAff_ (makeVar empty >>= freeFlow)

  where
    uiRunner :: UIRunner
    uiRunner screen = makeAff (\callback -> (\_ _ -> pure "") (Right >>> callback) screen *> pure nonCanceler)

    apiRunner :: APIRunner
    apiRunner request = makeAff (\callback -> (Right >>> callback) "Test" *> pure nonCanceler)

    permissionRunner :: PermissionRunner
    permissionRunner = PermissionRunner permissionCheckRunner permissionTakeRunner

    permissionCheckRunner :: PermissionCheckRunner
    permissionCheckRunner _ = makeAff (\callback -> (Right >>> callback) PermissionGranted *> pure nonCanceler)

    permissionTakeRunner :: PermissionTakeRunner
    permissionTakeRunner _ = makeAff (\callback -> (Right >>> callback) [] *> pure nonCanceler)

main :: forall e. Eff (AppEffects e) Unit
main = launchApp
